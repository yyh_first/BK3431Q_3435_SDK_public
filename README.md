# BK3431Q_3435_SDK_public

#### 介绍
BK3431Q、BK3435 SDK开发资料及开发工具

#### 软件架构
采用BLE stack rwip架构，内部运行ARM核


#### 安装教程

1. 请使用Keil MDK 5.12安装包，并安装support for ARM7/9 packet
2. 其他文件夹中的工具、源码均可直接使用

#### 使用说明

1.  APP_Notes：软件使用说明及指导文档
2.  HW：硬件参考设计
3.  SDK：软件源代码
4.  Tools：开发应用工具

#### 参与贡献

1.  创建人：林思明


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
